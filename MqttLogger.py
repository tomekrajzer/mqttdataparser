import base64
import json
import logging
import re
import traceback
from datetime import datetime

import paho.mqtt.client as mqtt_client
from influxdb import InfluxDBClient

from Credentials import mqtt, influx, lora

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', filename='MqttLogger.log', filemode='w')


class InfluxClient():
    def __init__(self):
        self.client = InfluxDBClient(host=influx['url'], port=influx['port'], username=influx['user'], password=influx['password'],
                                   database=influx['database'])

    def write_series(self, series):
        self.client.write_points(series)

    def get_measurement_value(self, measurement_name):
        query = "select last(value) from \"{measurement}\"".format(measurement=measurement_name)
        result = self.client.query(query, database=influx['database'])
        points = list(result.get_points())
        if len(points) > 0:
            return points[0]['last']
        return 0.0


class InfluxValue:
    def __init__(self, name, value):
        self.name = name
        self.value = value


class SonoffDS18B20:
    def __init__(self, sonoff_name, payload):
        self.name = sonoff_name
        self.temperature = InfluxValue(self.name + '_sensor_temperature', payload['DS18B20']['Temperature'])
        self.influx_client = InfluxClient()
        self.prepare_and_write_influx_series()

    def prepare_and_write_influx_series(self):
        measurements = vars(self)
        series = []
        for item in measurements:
            if not isinstance(measurements[item], InfluxValue):
                continue
            point_values = {
                "time": datetime.strftime(datetime.utcnow(), '%Y-%m-%dT%H:%M:%SZ'),
                "measurement": measurements[item].name,
                "fields": {
                    "value": measurements[item].value,
                },
                "tags": {
                    "deviceType": "SonoffTH",
                    "sensorType": "DS18B20"
                }
            }
            series.append(point_values)
        self.influx_client.write_series(series)


class SonoffSI7021:
    def __init__(self, sonoff_name, payload):
        self.name = sonoff_name
        self.temperature = InfluxValue(self.name + '_sensor_temperature', payload['SI7021']['Temperature'])
        self.humidity = InfluxValue(self.name + '_sensor_humidity', payload['SI7021']['Humidity'])
        self.influx_client = InfluxClient()
        self.prepare_and_write_influx_series()

    def prepare_and_write_influx_series(self):
        measurements = vars(self)
        series = []
        for item in measurements:
            if not isinstance(measurements[item], InfluxValue):
                continue
            point_values = {
                "time": datetime.strftime(datetime.utcnow(), '%Y-%m-%dT%H:%M:%SZ'),
                "measurement": measurements[item].name,
                "fields": {
                    "value": measurements[item].value,
                },
                "tags": {
                    "deviceType": "SonoffTH",
                    "sensorType": "SI7021"
                }
            }
            series.append(point_values)
        self.influx_client.write_series(series)


class SonoffPow:
    def __init__(self, sonoff_name, payload):
        self.name = sonoff_name
        self.power = InfluxValue(self.name + '_meter_watts', payload['ENERGY']['Power'])
        self.voltage = InfluxValue(self.name + '_meter_voltage', payload['ENERGY']['Voltage'])
        self.current = InfluxValue(self.name + '_meter_current', payload['ENERGY']['Current'])
        self.total = InfluxValue(self.name + '_meter_kwh', payload['ENERGY']['Total'])
        self.total_tmp = InfluxValue(self.name + '_meter_kwh_tmp', payload['ENERGY']['Total'])
        self.apparent_power = InfluxValue(self.name + '_meter_apparent_power', payload['ENERGY']['ApparentPower'])
        self.reactive_power = InfluxValue(self.name + '_meter_reactive_power', payload['ENERGY']['ReactivePower'])
        self.factor = InfluxValue(self.name + '_meter_factor', payload['ENERGY']['Factor'])
        self.influx_client = InfluxClient()
        self.compare_with_last_value()
        self.prepare_and_write_influx_series()

    def prepare_and_write_influx_series(self):
        measurements = vars(self)
        series = []
        for item in measurements:
            if not isinstance(measurements[item], InfluxValue):
                continue
            point_values = {
                "time": datetime.strftime(datetime.utcnow(), '%Y-%m-%dT%H:%M:%SZ'),
                "measurement": measurements[item].name,
                "fields": {
                    "value": measurements[item].value,
                },
                "tags": {
                    "deviceType": "SonoffPow"
                }
            }
            series.append(point_values)
        self.influx_client.write_series(series)
    
    def compare_with_last_value(self):
        last_total_value = self.influx_client.get_measurement_value(self.total.name)
        last_total_value_tmp = self.influx_client.get_measurement_value(self.total_tmp.name)

        if last_total_value > self.total.value > last_total_value_tmp:
            self.total.value = (self.total.value - last_total_value_tmp) + last_total_value
        elif last_total_value > self.total.value < last_total_value_tmp:
            self.total.value = last_total_value
        elif last_total_value <= self.total.value >= last_total_value_tmp:
            self.total.value = self.total.value
        else:
            self.total.value = last_total_value


class Node:
    def __init__(self, name, payload):
        self.device_name = name
        self.influx_client = InfluxClient()
        self.get_device_status(name, payload)

    def set_influx_measurements(self, payload):
        self.device_name = self.device_name
        self.active = InfluxValue(self.device_name + '_W', payload['active'])
        self.reactive = InfluxValue(self.device_name + '_Var', payload['reactive'])
        self.volt = InfluxValue(self.device_name + '_V', payload['voltage'])
        self.amp = InfluxValue(self.device_name + '_A', payload['amp'])
        self.total_active = InfluxValue(self.device_name + '_TkWh', payload['total_active'])
        self.total_reactive = InfluxValue(self.device_name + '_TkVar', payload['total_reactive'])

    def get_device_status(self, device_eui, data_b64):
        decode64 = base64.b64decode(data_b64)
        ba = bytearray(decode64)

        if list(ba)[1] != int(0x10):
            return

        payload = {'active': int.from_bytes(list(ba)[3:7], byteorder='little') / 100.0,
                   'reactive': int.from_bytes(list(ba)[7:11], byteorder='little') / 100.0,
                   'voltage': int.from_bytes(list(ba)[11:13], byteorder='little') / 10.0,
                   'amp': int.from_bytes(list(ba)[13:15], byteorder='little') / 1000.0,
                   'total_active': int.from_bytes(list(ba)[15:19], byteorder='little') / 1000.0,
                   'total_reactive': int.from_bytes(list(ba)[19:23], byteorder='little') / 1000.0}
        self.set_influx_measurements(payload)
        self.compare_with_last_value()
        self.prepare_influx_series()

    def compare_with_last_value(self):
        last_total_active = self.influx_client.get_measurement_value(self.total_active.name)
        last_total_reactive = self.influx_client.get_measurement_value(self.total_reactive.name)

        if last_total_active > self.total_active.value:
            self.total_active.value = last_total_active + self.total_active.value
        if last_total_reactive > self.total_reactive.value:
            self.total_reactive.value = last_total_reactive + self.total_reactive.value

    def prepare_influx_series(self):
        measurements = vars(self)
        series = []
        for item in measurements:
            if not isinstance(measurements[item], InfluxValue):
                continue
            point_values = {
                "time": datetime.strftime(datetime.utcnow(), '%Y-%m-%dT%H:%M:%SZ'),
                "measurement": measurements[item].name,
                "fields": {
                    "value": measurements[item].value,
                },
                "tags": {
                    "deviceType": "ResNode"
                }
            }
            series.append(point_values)
        self.influx_client.write_series(series)


def on_connect(client, userdata, flags, rc):
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("#")


def on_message(client, userdata, msg):
    topic = str(msg.topic)
    if topic.startswith('tele') and topic.endswith('SENSOR'):
        sonoff_name = re.search('tele/(.*)/SENSOR', msg.topic).group(1)
        message = msg.payload.decode('utf-8')
        data_json = json.loads(message)
        try:
            if ('ENERGY' in data_json and '17' in sonoff_name):
                SonoffPow(sonoff_name, data_json)
            elif ('SI7021' in data_json):
                SonoffSI7021(sonoff_name, data_json)
            elif ('DS18B20' in data_json):
                SonoffDS18B20(sonoff_name, data_json)
        except Exception as e:
            logging.error("SONOFF NAME - " + " " + sonoff_name + "\n" + traceback.format_exc())
    elif topic.startswith('application/{appId}/device'.format(appId = lora['application_id'])):
        device_eui = re.search('device/(.*)/rx', msg.topic).group(1)
        message = msg.payload.decode('utf-8')
        data_json = json.loads(message)
        data_b64 = data_json['data']
        try:
            Node(device_eui, data_b64)
        except:
            logging.error("ResNode NAME - " + " " + device_eui + "\n" + traceback.format_exc())


def main():
    client = mqtt_client.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.username_pw_set(mqtt['user'], mqtt['password'])

    client.connect(mqtt['url'], mqtt['port'],)

    client.loop_forever()


if __name__ == '__main__':
    main()
